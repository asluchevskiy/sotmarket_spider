# -*- coding: utf-8 -*-
"""
Developed by Arseniy Sluchevskiy <arseniy.sluchevskiy@gmail.com>
"""
import logging
import optparse
import sys
import locale
import os
from grab.tools.lock import assert_lock
import spiders
from spiders import settings

if __name__ == '__main__':
    reload(sys)
    sys.setdefaultencoding(locale.getpreferredencoding())

    logging.basicConfig(level=settings.LOG_LEVEL)
    parser = optparse.OptionParser("usage: %prog [options]")
    parser.add_option('-m', '--mode', action='store', dest='mode', default='parse', help='parse|export|all')
    (options, args) = parser.parse_args()

    if options.mode in ('all', 'parse'):
        spiders.SotMarketXMLSpider(thread_number=settings.THREADS).run()
        spiders.SotMarketListSpider(thread_number=settings.THREADS).run()
        spiders.SotMarketSpider(thread_number=settings.THREADS).run()
    elif options.mode in ('all', 'export'):
        b = spiders.SotMarketSpider(thread_number=settings.THREADS)
        b.prepare()
        b.export_csv()
    if options.mode not in ('all', 'export', 'parse'):
        parser.print_help()