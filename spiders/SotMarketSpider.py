# -*- coding: utf-8 -*-
"""
Developed by Arseniy Sluchevskiy <arseniy.sluchevskiy@gmail.com>
"""
import os
import re
import json
import logging
import string
import pymongo
from datetime import datetime
from grab import Grab
from grab.spider import Task, Spider
from grab.selector.selector import DataNotFound
from grab.tools.text import normalize_space, find_number
from spiders import settings, BaseSpider
from pyquery import PyQuery
from urlparse import urlsplit

class SotMarketXMLSpider(Spider):

    initial_urls = ['http://sotmarket.ru']
    db = pymongo.Connection()[settings.MONGO_DB]

    def task_initial(self, grab, task):
        g = Grab(timeout=120)
        g.setup(url='http://xml.sotmarket.ru/xml/xml_all.xml')
        # yield Task('xml', grab=g)
        yield Task('categories_xml', 'http://xml.sotmarket.ru/xml/categories.xml')
        yield Task('manufacturers_xml', 'http://xml.sotmarket.ru/xml/manufacturers.xml')

    def task_categories_xml(self, grab, task):

        def parse_category(category, path):
            name = category.attr('name')
            _id = int(category.attr('id'))
            path = path + [name]
            if not self.db.category.find_one({'_id': _id}):
                self.db.category.insert({'_id': _id, 'name': name, 'path': path, 'url': category.attr('url')})
            for c in category.select('./category'):
                parse_category(c, path)
        for category in grab.doc.select('//categories/category'):
            parse_category(category, [])

    def task_manufacturers_xml(self, grab, task):
        for m in grab.doc.select('//manufacturer'):
            if not self.db.manufacturer.find_one({'_id': int(m.attr('id'))}):
                self.db.manufacturer.insert({'_id': int(m.attr('id')), 'name': m.attr('name')})

    def task_xml(self, grab, task):
        for offer in grab.doc.select('//offers/offer'):
            _id = int(offer.attr('id'))
            # is_available = True if offer.attr('available') == 'true' else False
            url = offer.select('./url').text()
            name = offer.select('./name').text()
            price = offer.select('./price').number()
            category_id = offer.select('./categoryid').number()
            short_description = offer.select('./description').text()
            # image_small = offer.select('./picture').text()
            item = self.db.item.find_one({'_id': _id})
            if not item:
                data = dict(_id=_id, flags=dict(is_parsed=False, is_downloaded=False), url=url, price=price,
                            name=name, category_id=category_id, short_description=short_description,
                            ctime=datetime.now(), mtime=datetime.now())
                self.db.item.insert(data)
            elif item['price'] != price:
                self.db.item.update({'_id': _id}, {'$set': {'price': price}})

class SotMarketListSpider(BaseSpider):

    def task_generator(self):
        # self.setup_grab(log_dir='logs')
        self.setup_grab(cookies=dict(ipzone='1-1-77-0-0-0'))
        for cat in self.db.category.find({}, timeout=False):
            url = 'http://www.sotmarket.ru/category/%s.html' % cat['url']
            baseurl = 'http://www.sotmarket.ru/category/' + cat['url'] + '/pagenum-%s.html'
            yield Task('list', url, baseurl=baseurl, p=1)
            #break

    def task_list(self, grab, task):
        pq = PyQuery(grab.tree)
        li_list = pq.find('li.l-catalog-item')
        for i in range(len(li_list)):
            _id = int(li_list.eq(i).attr('data-id'))
            a = li_list.eq(i).find('div.b-catalog_goods-link a')
            name = a.text()
            url = grab.make_url_absolute(a.attr('href'))
            span_list = li_list.eq(i).find('span.mod_price')
            price = None
            availability = u'не доступен'
            for j in range(len(span_list)):
                if span_list.eq(j).hasClass('scheme_available'):
                    availability = u'есть на складе'
                elif span_list.eq(j).hasClass('scheme_request'):
                    availability = u'под заказ'
                else:
                    continue
                price = find_number(span_list.eq(j).text(), ignore_spaces=True)

            item = self.db.item.find_one({'_id': _id})
            if not item:
                self.db.item.save(dict(_id=_id, url=url, price=price, availability=availability, name=name,
                                       mtime=datetime.now(), ctime=datetime.now()))
            if item and (item['price'] != price or item['availability'] != availability):
                self.db.item.update({'_id': item['_id']}, {'$set': dict(price=price, availability=availability,
                                    mtime=datetime.now(), flags=dict(is_parsed=False, is_downloaded=False))})

        if grab.doc.select(u'//span[@class="a-link" and text()="Вперёд"]'):
            url = task.baseurl % (task.p+1,)
            yield Task('list', url, baseurl=task.baseurl, p=task.p+1)
        if pq.find('div.b-showmissing'):
            yield Task('list', grab.make_url_absolute(pq.find('div.b-showmissing a').attr('href')),
                       baseurl=task.baseurl, p=task.p+1)

class SotMarketSpider(BaseSpider):

    def task_generator(self):
        self.setup_grab(cookies=dict(ipzone='1-1-77-0-0-0'))
        self.setup_grab(timeout=180)
        for item in self.db.item.find({'$or': [{'flags.is_parsed': False}, {'flags.is_downloaded': False}]},
                                      timeout=False).limit(1000):
            if not item['flags'].get('is_parsed'):
                yield Task('page', item['url'], item=item)
            elif not item['flags'].get('is_downloaded'):
                self.download_content(item)
        # yield Task('page', 'http://www.sotmarket.ru/product/apple-ipad-mini-4g-64gb.html')

    images_counter = dict()
    images = dict()
    files = dict()
    files_counter = dict()

    def download_content(self, item):
        item_id = item['_id']
        self.images_counter[item_id] = len(item['images'])
        self.files_counter[item_id] = len(item['files'])
        self.files[item_id] = item['files']
        for i in range(len(item['files'])):
            self.add_task(Task('file', item['files'][i]['url'], i=i, item_id=item_id, disable_cache=True))
        for i in range(len(item['images'])):
            self.add_task(Task('image', item['images'][i], i=i, item_id=item_id, disable_cache=True))
        self.check_download_done(item_id)

    def task_page(self, grab, task):
        if grab.response.code != 200:
            logging.error('Bad response code: %s' % task.url)
            return
        
        availability = None
        for selector in ('//p[@class="b-goods-price-availability"]', '//td[@class="b-goods-price-content"]'):
            try:
                if not availability:
                    availability = grab.doc.select(selector).text().lower()
            except DataNotFound:
                pass
        # logging.debug(availability)

        files = list()
        for div in grab.doc.select(u'//div[@class="b-text-files"]'):
            name = div.select('./h3').text()
            url = div.select('.//a').attr('href')
            files.append(dict(name=name, url=url))


        description = None
        for selector in (u'//div[@class="l-content-h"]//div[@class="b-text"]',
                         u'//div[@class="b-product-info-cell" and contains(.//div[@class="b-title"], "Описание")]'):
            try:
                if not description:
                    description = grab.doc.select(selector).node()
            except DataNotFound:
                pass
        if description:
            description = PyQuery(description).remove('h2').remove('div').remove('span').html().strip()
        # logging.debug(description)

        try:
            accessories = grab.doc.select(u'//div[@class="b-product-info-cell" and contains(.//div[@class="b-title"], "Комплектация")]').select('.//div[@class="b-text"]').text()
        except (AttributeError, DataNotFound):
            accessories = None
        # logging.debug(accessories)

        variants = [re.sub('\(.+?\)', '', li.text()).strip() for li in grab.doc.select('//div[@id="stm-options"]//li')]

        properties = list()
        for div in grab.doc.select('///div[@class="b-goods-specifications-item"]'):
            try:
                properties_title = div.select('./p[@class="b-goods-specifications-title"]').text()
            except DataNotFound:
                properties_title = u'Общие характеристики'
            properties_data = list()
            for li in div.select('.//li'):
                key = li.select('.//span').text()
                value = li.select('.//div[2]').text()
                properties_data.append(dict(key=key, value=value))
            properties.append(dict(title=properties_title, data=properties_data))

        images = [img.attr('src').replace('65x65/', '') for img in grab.doc.select('//div[@id="stm-gallery"]//img')]

        article = grab.doc.select('//span[@itemprop="sku"]').text()

        try:
            title = grab.doc.select('//div[@class="b-title-techspec"]').text()
        except DataNotFound:
            title = None
        # logging.debug(title)

        path = [span.text() for span in grab.doc.select('//div[@class="b-breadcrumbs"]//span')]
        try:
            brand = grab.doc.select('//span[@itemprop="brand"]').text()
        except DataNotFound:
            brand = None
        if not self.db.manufacturer.find_one({'name': brand}):
            brand = None
        if brand and path:
            path = path[:-1]
        # logging.debug(string.join(path, ', '))

        task.item.update(dict(mtime=datetime.now(), path=path, description=description, article=article, files=files,
                              accessories=accessories, properties=properties, images=images, title=title,
                              variants=variants, availability=availability, brand=brand,
                              flags=dict(is_parsed=True, is_downloaded=False)))
        self.db.item.save(task.item)
        # downlooooooad content
        self.download_content(task.item)

        # js_url = re.search('"(http:\/\/front.+?js)"', grab.response.body).group(1)
        # yield Task('js', js_url, item=task.item)

    # def task_js(self, grab, task):
        # json_data = re.search('^Stm.store.seoHash = ({.+?});$', grab.response.body, flags=re.DOTALL).group(1)
        # data = json.loads(json_data.replace('\'', '"'))

    def check_download_done(self, item_id):
        if not self.files_counter[item_id] and not self.images_counter[item_id]:
            images = [self.images[item_id][key] for key in self.images.get(item_id, [])]
            self.db.item.update({'_id': item_id}, {'$set': {'flags.is_downloaded': True,
                                                            'files': self.files[item_id],
                                                            'images_local': images}})

    # todo: объединить image & file в один task download

    def task_image(self, grab, task):
        if grab.response.code == 200:
            local_path = urlsplit(task.url).path[1:]
            path = os.path.join(settings.DATA_DIR, local_path)
            grab.response.save(path, create_dirs=True)
            if not task.item_id in self.images:
                self.images[task.item_id] = dict()
            self.images[task.item_id][task.i] = local_path
        elif grab.response.code == 404:
            pass
        else:
            logging.debug('Response code %s: %s' % (grab.response.code, task.url))
            return
        self.images_counter[task.item_id] -= 1
        self.check_download_done(task.item_id)

    def task_file(self, grab, task):
        if grab.response.code == 200:
            local_path = urlsplit(task.url).path[1:]
            path = os.path.join(settings.DATA_DIR, local_path)
            grab.response.save(path, create_dirs=True)
            self.files[task.item_id][task.i]['path'] = local_path
        elif grab.response.code == 404:
            # self.files[task.item_id][task.i]['path'] = None
            pass
        else:
            logging.debug('Response code %s: %s' % (grab.response.code, task.url))
            return
        self.files_counter[task.item_id] -= 1
        self.check_download_done(task.item_id)

    def export_json(self):
        import json
        dthandler = lambda obj: obj.isoformat() if isinstance(obj, datetime) else None
        data = list(self.db.item.find({'flags.is_parsed': True}, timeout=False))
        f = open('data.json', 'w')
        json.dump(data, f, ensure_ascii=False, indent=4, default=dthandler)
        f.close()

    def export_csv(self):
        import csv
        max_images = 25
        f = open('result.csv', 'w')
        properties = self.db.item.distinct('properties.data.key')
        fieldnames = ['ID', 'NAME', 'TITLE', 'PREVIEW_TEXT', 'DESCRIPTION', 'PRICE', 'BRAND',
                      'AVAILABILITY', 'ARTICLE', 'ACCESSORIES', 'DATASHEET',
                      'GROUP0', 'GROUP1', 'GROUP2', 'GROUP3', 'GROUP4'] + \
                     ['IMAGE%s' % i for i in range(max_images)] + properties
        w = csv.DictWriter(f, fieldnames, delimiter=';', quoting=csv.QUOTE_NONNUMERIC)
        w.writeheader()
        for item in self.db.item.find({'flags.is_parsed': True}, timeout=False):
            data = dict(ID=item['_id'], NAME=item['name'], TITLE=item['title'], BRAND=item.get('brand'),
                        DESCRIPTION=normalize_space(item['description']),
                        PRICE=item['price'], AVAILABILITY=item['availability'], ARTICLE=item['article'],
                        ACCESSORIES=item['accessories'], DATASHEET=item.get('datasheet_local'))
            images = item.get('images_local', [])
            for i in range(len(images)):
                if i >= max_images:
                    break
                data['IMAGE%s' % i] = images[i]
            for group in item['properties']:
                for p in group['data']:
                    data[p['key']] = p['value']
            category = self.db.category.find_one({'_id': item['category_id']})
            for i in range(len(category['path'])):
                data['GROUP%s' % i] = category['path'][i]
            w.writerow(data)
        f.close()
