# -*- coding: utf-8 -*-
"""
Developed by Arseniy Sluchevskiy <arseniy.sluchevskiy@gmail.com>
"""
from spiders import settings
import signal
import logging
import pymongo
import hashlib
from grab.spider import Spider
import os

class BaseSpider(Spider):
    db = pymongo.Connection()[settings.MONGO_DB]

    def shutdown(self):
        print self.render_stats()

    def sigint_handler(self, signal, frame):
        logging.info('Interrupting...')
        self.work_allowed = False

    def prepare(self):
        signal.signal(signal.SIGINT, self.sigint_handler)
        if settings.PROXY_LIST:
            self.load_proxylist(settings.PROXY_LIST, 'text_file', auto_change=True)
        if settings.USE_CACHE:
            self.setup_cache(database=settings.MONGO_DB)

    def shutdown(self):
        logging.info('Job done!')