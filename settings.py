# -*- coding: utf-8 -*-
"""
Developed by Arseniy Sluchevskiy <arseniy.sluchevskiy@gmail.com>
"""
import os
import logging

# PROXY_LIST = '/home/arceny/Dropbox/develop/proxy.list'
PROXY_LIST = None
# USE_CACHE = True
USE_CACHE = False
MONGO_DB = 'sotmarket'
LOG_LEVEL = logging.DEBUG
DEBUG = True
THREADS = 16
DATA_DIR = '/home/arceny/work/sotmarket'
